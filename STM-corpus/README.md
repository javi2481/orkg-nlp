# STM-corpus: Domain-independent Extraction of Scientific Concepts from Research Articles

This repository contains the annotated corpus and source code for the paper:

Brack A., D’Souza J., Hoppe A., Auer S., Ewerth R. (2020) Domain-Independent Extraction of Scientific Concepts from Research Articles. In: Jose J. et al. (eds) Advances in Information Retrieval. ECIR 2020. Lecture Notes in Computer Science, vol 12035. Springer, Cham
- Open access:
https://link.springer.com/chapter/10.1007/978-3-030-45439-5_17

- Pre-print: 
https://arxiv.org/abs/2001.03067


## Installation

### Requirements
- setup python 3.7.5 environment
- install the required python packages with 'pip install -r requirements.txt' 
- download spacy tokenisation model with 'python -m spacy download en_core_web_md'
- Please note: the python scripts below call python commands. If you use virtual environments, then you have to adapt the path to the python command that is located in the virtual environemnt (e.g. venv/bin/python).

### SciBERT
- Download SciBERT from https://github.com/allenai/scibert (Pytorch HuggingFace, scibert-scivocab-uncased )
- extract all files into the directory "bert_model/scibert_scivocab_uncased/"

## STM-corpus
The annotated corpus is located in the folder data/dataset_v2 with the following structure:
- origin: contains the corpus separated per domain in BRAT/standoff format
- folds: contains the 11 folds (first 5 are used for evaluation) with the following structure:
-- fold_[fold number]/conll/[train|dev|test]/[domain|overall]: contains tokenized data in the CONLL format for each domain and overall
-- fold_[fold number]/standoff/[train|dev|test]/[domain|overall]: contains origin data in the BRAT/standoff format for each domain and overall
- The folds can be recreated from the origin data with the python script 'stm_build_dataset.py'
- The *.csv files contain several characteristics about the corpus. These can be recreated with the script 'stm_stats.py'
- The file 'Annotation Guidelines.pdf' contains the annotation guidelines

## Base Classifier

### Hyperparameter tuning
To perform a hyperparameter-tuning for best learning rate, dropout and LSTM hidden size for all domains 
and overall classifier do the following:

- open the file 'scibert_allennlp_stm_per_domain_hyperparam_tuning.py'
- go through the "# ADAPT:" comments and adapt the settings
- if you only want to train a certain domain with certain hyperparameters than just adapt the 'hyper_params' dictionary accordingly
- execute the script with python
- the script trains for each domain, each fold and hyperparameter combination a model with allennlp
- the allennlp model configuration 'allennlp_config/ner_hyperparam.json' is used for the training
- training is performed concurrently on several GPUs (if available), please adapt the 'gpus' variable accordingly
- all experiment results are stored in a separate folder under "results/"

To determine the best hyperparameters based on the allennlp span-based dev f1-score:
- open the jupyter notebook 'scibert_allennlp_stm_hyperparam_eval_allennlp_only.ipynb'
- go through the "# ADAPT:" comments and adapt the settings
- execute the jupyter notebook
- the notebook creates a csv file 'results/hyperparams_per_domain' which contains the best hyperparameter values for all domains
- our best determined hyperparameters are available in file 'data/dataset_v2/hyperparams_per_domain.csv'

In order to run the predictions and evaluate the models further: 
- move best models (manually) to a different folder, e.g. move 'overall_dr_0.5_lstm_hs_768_lr_0.005' to 'results/stm_v2_best_models'
- remove the other models to save space

### Run Predictions
The performance of the models is evaluated with the  official ScienceIE-17 evaluation script which is based on the standoff/BRAT file format.
For that we have to perform the predictions in the standoff format. We do this only for the best models as it takes some time.
Do the following to run the predictions
- open the file 'scibert_allennlp_stm_predict.py'
- go through the "# ADAPT:" comments and adapt the settings
- the script writes the predictions and calculates several metrics for each fold and domain in the resp. folders
- the calculated metrics are stored in the file 'metrics_scienceie2017.json' in the resp. folders

### Evaluate Metrics
Next, we have to aggregate the metrics for each domain and fold. 

- open the jupyter notebook 'scibert_allennlp_stm_best_eval.ipynb'
- go through the "# ADAPT:" comments and adapt the settings
- run the notebook
- the notebook creates a csv file 'results/{run}/results.csv' which contains aggregated metrics for the resp. domains.
- besides, the notebook creates confusion matrices in the resp run folders.






