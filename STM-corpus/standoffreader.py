"""
Parser for the BRAT standoff file format.

Code partially partially adopted from https://github.com/Franck-Dernoncourt/NeuroNER/blob/master/neuroner/brat_to_conll.py
and
the utility scripts of https://scienceie.github.io/resources.html
"""

import sys
import os
import spacy
import re

OTHER_TOKEN = "O"


class Document:

    def __init__(self, sentences, text, basename):
        self.sentences = sentences
        self.text = text
        self.basename = basename

    def get_sentence_count(self):
        return len(self.sentences)


class Entity:

    def __init__(self, label, start, end, text):
        self.label = label
        self.start = start
        self.end = end
        self.text = text

    def __str__(self):
        return str(self.label) + " " + str(self.start) + " " + str(self.end) + " "+ self.text

    def __repr__(self):
        return self.__str__()

    def to_standoff_str(self, entity_num):
        return 'T{0}\t{1} {2} {3}\t{4}\n'.format(entity_num, self.label, self.start, self.end, self.text)


class Token:

    def __init__(self, labels, start, end, text):
        self.labels = labels
        self.start = start
        self.end = end
        self.text = text

    def assign_new_labels(self, new_labels):
        return Token(new_labels, self.start, self.end, self.text)

    def get_token_of_type(self, token_type):
        for l in self.labels:
            if token_type in l:
                return l
        return None

    def __str__(self):
        if self.start < 0 and self.end < 0:
            return str(self.labels) + " " + self.text
        else:
            return str(self.labels) + " " + str(self.start) + " " + str(self.end) + " "+ self.text

    def __repr__(self):
        return self.__str__()


def parse_entities(entities_text, ignore_entities=[]):
    result = []
    for entity_line in entities_text.splitlines():
        anno_inst = entity_line.strip().split("\t")
        if len(anno_inst) == 3:
            entity_type = anno_inst[0].strip()
            anno_inst1 = anno_inst[1].split(" ")
            if len(anno_inst1) == 3:
                keytype, start, end = anno_inst1
            else:
                keytype, start, _, end = anno_inst1
            if entity_type.startswith("T"):
                keyphr_ann = anno_inst[2].strip()
                if keytype in ignore_entities:
                    continue
                result.append(Entity(keytype, int(start), int(end), keyphr_ann))
    return result


def testOverlap(x1, x2, y1, y2):
  return (x1 >= y1 and x1 <= y2) or (x2 >= y1 and x2 <= y2) or (y1 >= x1 and y1 <= x2) or (y2 >= x1 and y2 <= x2)


def remove_overlapping_entitities(entities, keep_only_one_label=False):
    """ When two entity annotations of the same entity type overlap, we only keep the longer one. """
    eliminate = {}

    for t1 in entities:
        for t2 in entities:
            if t1 is t2:
                continue

            if not keep_only_one_label and t1.label != t2.label:
                continue

            if testOverlap(t1.start, t1.end, t2.start, t2.end):
                if t1.end - t1.start >= t2.end - t2.start:
                    eliminate[t2] = True
                    print("WARNING: eliminate %s due to overlap with %s" % (t2, t1))
                else:
                    eliminate[t1] = True
                    print("WARNING: eliminate %s due to overlap with %s" % (t1, t2))

    return [t for t in entities if t not in eliminate]

def check_text_and_entities_match(text, annotations):
    for anno in annotations:
        start = anno.start
        end = anno.start + len(anno.text)
        text_lookup = text[start:end]
        if text_lookup != anno.text:
            print("ERROR: annotated phrase does not equal text:" + text_lookup + " != " + anno.text)
            #raise IOError("ERROR: annotated phrase does not equal text:" + text_lookup + " != " + anno.text)


def get_start_and_end_offset_of_token_from_spacy(token):
    start = token.idx
    end = start + len(token)
    return start, end


def get_sentences_and_tokens_from_spacy(text, spacy_nlp):

    document = spacy_nlp(text)
    # sentences
    sentences = []
    for span in document.sents:
        sentence = [document[i] for i in range(span.start, span.end)]
        sentence_tokens = []
        for token in sentence:
            token_dict = {}
            token_dict['start'], token_dict['end'] = get_start_and_end_offset_of_token_from_spacy(token)
            token_dict['text'] = text[token_dict['start']:token_dict['end']]
            if token_dict['text'].strip() in ['\n', '\t', ' ', '']:
                continue
            # Make sure that the token text does not contain any space
            if len(token_dict['text'].split(' ')) != 1:
                print("WARNING: the text of the token contains space character, replaced with hyphen\n\t{0}\n\t{1}".format(token_dict['text'],
                                                                                                                           token_dict['text'].replace(' ', '-')))
                token_dict['text'] = token_dict['text'].replace(' ', '-')
            sentence_tokens.append(token_dict)
        sentences.append(sentence_tokens)
    return sentences


def annotate_sentences(sentences, entities):
    annotated_sentences = []

    entities = sorted(entities, key=lambda e: e.start)

    for sentence in sentences:
        annotated_sentence = []
        for token in sentence:
            gold_labels = []

            for entity in entities:
                if entity.start <= token['start'] < entity.end or \
                        entity.start < token['end'] <= entity.end or \
                        token['start'] < entity.start < entity.end < token['end']:

                    if entity.label != OTHER_TOKEN:
                        if token['start'] <= entity.start:
                            # begin of entity
                            if not 'I-{0}'.format(entity.label) in gold_labels:
                                gold_labels.append('B-{0}'.format(entity.label))
                        else:
                            # inside of entity
                            if not 'B-{0}'.format(entity.label) in gold_labels:
                                gold_labels.append('I-{0}'.format(entity.label))

                elif token['end'] < entity.start:
                    break

            if len(gold_labels) == 0:
                gold_labels.append(OTHER_TOKEN)

            set_gold_labels = sorted(list(set(gold_labels)))
            if set_gold_labels != sorted(gold_labels):
                print("WARNING: several redundant gold labels: " + str(gold_labels) + " of token " + str(token))
                gold_labels = set_gold_labels

            annotated_token = Token(gold_labels, token["start"], token["end"], token["text"])
            annotated_sentence.append(annotated_token)

        annotated_sentences.append(annotated_sentence)

    return annotated_sentences

def bio_to_bioul(sentence):
    result = []

    last_token_bio = None
    state = None
    last_token = None
    last_token_label = None
    for t in sentence:
        l = t.labels[0]
        if l == 'O':
            bio = l
            label = l
        else:
            bio, label = l.split('-')

        assert bio in ["B", "I", "O"]
        assert state is None or state in ["B", "I", "O"]

        if state is None:
            if bio == "B" or bio == "I":
                last_token_bio = "U"
                state = "B"
            else:
                last_token_bio = "O"
                state = "O"

        elif state == "O":
            if bio == "B" or bio == "I":
                last_token_bio = "O"
                state = "B"
            else:
                last_token_bio = "O"
                state = "O"

        elif state == "B":
            if bio == "B" or bio == "O":
                last_token_bio = "U"
                state = "B"
            else:
                last_token_bio = "B"
                state = "I"
        else:
            if bio == "I":
                last_token_bio = "I"
                state = "I"
            elif bio == "B":
                last_token_bio = "L"
                state = "B"
            else:
                last_token_bio = "L"
                state = "O"

        if last_token is not None:
            if last_token_bio == "O" or last_token_label == "O":
                result.append(last_token.assign_new_labels(["O"]))
            else:
                result.append(last_token.assign_new_labels([last_token_bio + "-" + last_token_label]))

        last_token = t
        last_token_label = label

    if last_token is not None:
        if state == "B":
            last_token_bio = "U"
        elif state == "I":
            last_token_bio = "L"
        else:
            last_token_bio = "O"

        if last_token_bio == "O" or last_token_label == "O":
            result.append(t.assign_new_labels(["O"]))
        else:
            result.append(t.assign_new_labels([last_token_bio + "-" + last_token_label]))

    assert len(result) == len(sentence)
    return result


def process_standoff_file(text, annotations, spacy_nlp, keep_only_one_label=False, bioul=False,
                          ignore_entities=[]):
    annotations = parse_entities(annotations, ignore_entities=ignore_entities)

    check_text_and_entities_match(text, annotations)

    annotations = remove_overlapping_entitities(annotations, keep_only_one_label)

    sentences = get_sentences_and_tokens_from_spacy(text, spacy_nlp)

    annotated_sentences = annotate_sentences(sentences, annotations)
    if not bioul:
        return annotated_sentences
    else:
        result = []
        for s in annotated_sentences:
            s_bioul = bio_to_bioul(s)
            result.append(s_bioul);
        return result


def create_spacy_tokenizer(model="en_core_web_md"):
    # en_core_web_md seems to be better that web_sm, see https://spacy.io/models/en#en_core_web_md
    spacy_nlp = spacy.load(model)
    return spacy_nlp


def process_standoff_files(folder, keep_only_one_label=False, max_documents=-1, bioul=False, spacy_nlp=None,
                           ignore_entities=[]):
    if spacy_nlp is None:
        #spacy_nlp = create_spacy_tokenizer("en_core_web_sm")
        spacy_nlp = create_spacy_tokenizer()

    result = []
    flist = os.listdir(folder)
    count = 0
    for f in flist:
        if not f.endswith(".ann"):
            continue

        if max_documents >= 0 and count >= max_documents:
            print("Read max files: " + str(max_documents))
            break

        count += 1
        print("Processing file: " + str(f))

        with open(os.path.join(folder, f), "r", encoding="utf-8") as f_anno:
            anno = f_anno.read()

        with open(os.path.join(folder, f.replace(".ann", ".txt")), "r", encoding="utf-8") as f_text:
            text = f_text.read()

        # replace new lines with space
        text = text.replace('\r\n', ' ')
        text = text.replace('\n', ' ')
        text = text.replace('\r', ' ')

        sentences = process_standoff_file(text, anno, spacy_nlp, keep_only_one_label, bioul=bioul, ignore_entities=ignore_entities)
        basename = f.replace(".ann", "")

        result.append(Document(sentences, text, basename))
    return result


def get_possible_labels(document):
    labels = []
    for sentence in document:
        for word in sentence:
            for l in word.labels:
                # remove prefix
                s = l.split("-")
                if len(s) == 2:
                    if not s[1] in labels:
                        labels.append(s[1])

    return labels


def is_begin_of_label(word, label):
    return "B-" + label in word.labels or "U-" + label in word.labels


def is_inside_of_label(word, label):
    return "I-" + label in word.labels or "L-" + label in word.labels


def is_outside_of_label(word, label):
    return not is_begin_of_label(word, label) and not is_inside_of_label(word, label)


def sentences_to_entities(orig_text, sentences):
    result = []
    for label in get_possible_labels(sentences):
        entity_tokens = []
        for sentence in sentences:
            for token in sentence:
                if len(entity_tokens) > 0:
                    # inside of entitiy
                    if is_begin_of_label(token, label) or is_outside_of_label(token, label):
                        # end of entity
                        first_entity_token = entity_tokens[0]
                        last_entity_token = entity_tokens[-1]
                        if orig_text is None:
                            entity_text = " ".join([t.text for t in entity_tokens])
                        else:
                            entity_text = orig_text[first_entity_token.start:last_entity_token.end]

                        result.append(Entity(label, first_entity_token.start, last_entity_token.end, entity_text))

                        if is_begin_of_label(token, label):
                            # new entity started
                            entity_tokens = []
                            entity_tokens.append(token)
                        else:
                            # now outside of entity
                            entity_tokens = []
                    else:
                        # still inside of entity
                        entity_tokens.append(token)
                else:
                    # outside of entity
                    if is_begin_of_label(token, label) or is_inside_of_label(token, label):
                        # new entity started
                        entity_tokens = []
                        entity_tokens.append(token)
                    else:
                        # still outside
                        assert(len(entity_tokens) == 0)

        if len(entity_tokens) > 0:
            # finish entity
            first_entity_token = entity_tokens[0]
            last_entity_token = entity_tokens[-1]
            if orig_text is None:
                entity_text = " ".join([t.text for t in entity_tokens])
            else:
                entity_text = orig_text[first_entity_token.start:last_entity_token.end]

            result.append(Entity(label, first_entity_token.start, last_entity_token.end, entity_text))

    return result


def write_standoff_file(orig_text, document_sentences):

    entities = sentences_to_entities(orig_text, document_sentences)
    result = write_entities(entities)
    return result


def write_to_conll(documents):
    """ Writes the Documents to conll format.
    Each document starts with -DOCSTART - -X - -X - O
    Each token is representend as text, document_file_basenem, start, end, labels
    In multilabel setting labels are joined with '_'
    """

    result = ""
    for d in documents:
        result += "-DOCSTART - -X - -X - O\n\n"
        for s in d.sentences:
            for t in s:
                labels_str = "_".join(sorted(t.labels))
                token_str = '{0} {1} {2} {3} {4}\n'.format(t.text, d.basename, t.start, t.end, labels_str)
                result += token_str
            result += "\n"

    return result


def write_to_conll_allennlp(documents, concat_sentences=False):
    """ Writes the Documents to conll format for AllenNLP.
    """
    result = ""
    for d in documents:
        result += "-DOCSTART- (" + d.basename + ")\n\n"
        tokens = 0
        for s in d.sentences:
            for t in s:
                if len(t.labels) > 1:
                    print(f"ERROR: token in file {d.basename} has several labels: {t}")
                labels_str = t.labels[0]
                token_str = '{0}\t{1},{2}\tO\t{3}\n'.format(t.text, t.start, t.end, labels_str)
                result += token_str
                tokens += 1
            if not concat_sentences:
                result += "\n"
    return result


def read_conll_allennlp(file):
    result = []
    with open(file, "r", encoding="utf-8") as f:
        cur_doc = None
        cur_sentence = []
        for line in f.readlines():
            match = re.match("-DOCSTART- \\((.*)\\)", line)
            if match:
                # new document
                basename = match.group(1)
                cur_doc = Document(sentences=[], text=None, basename=basename)
                result.append(cur_doc)
                continue
            elif line.startswith("-DOCSTART-"):
                cur_doc = Document(sentences=[], text=None, basename=f'doc_{len(result)}')
                result.append(cur_doc)
                continue

            if line.strip() == "":
                # new sentence
                if len(cur_sentence) > 0:
                    cur_doc.sentences.append(cur_sentence)
                    cur_sentence = []
                continue

            match = re.match("(.+)\t(\\d+),(\\d+)\tO\t(.+)", line)
            if match:
                # new token
                token_text = match.group(1)
                start = int(match.group(2))
                end = int(match.group(3))
                label = match.group(4)
                t = Token(labels=[label], start=start, end=end, text=token_text)
                cur_sentence.append(t)
                continue

            match = re.match("(.+) (.+) (.+) (.+)", line)
            if match:
                # new token
                token_text = match.group(1)
                start = -1
                end = -1
                label = match.group(4)
                t = Token(labels=[label], start=start, end=end, text=token_text)
                cur_sentence.append(t)
                continue

    return result


def write_entities(entities):
    result = ""
    for i, e in enumerate(entities):
        result += e.to_standoff_str(i + 1)
    return result
