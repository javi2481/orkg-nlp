-DOCSTART- (S0016236113008041)

Elemental	0,9	O	B-Process
partitioning	10,22	O	I-Process
,	22,23	O	O
including	24,33	O	O
gaseous	34,41	O	B-Material
elemental	42,51	O	I-Material
emissions	52,61	O	I-Material
from	62,66	O	O
pilot	67,72	O	B-Data
scale	73,78	O	I-Data
(	79,80	O	I-Data
25kWth	80,86	O	I-Data
)	86,87	O	I-Data
,	87,88	O	O
post	89,93	O	B-Process
combustion	94,104	O	I-Process
CO2	105,108	O	I-Process
capture	109,116	O	I-Process
using	117,122	O	O
a	123,124	O	B-Material
Ca	125,127	O	I-Material
-	127,128	O	I-Material
based	128,133	O	I-Material
sorbent	134,141	O	I-Material
,	141,142	O	O
have	143,147	O	O
been	148,152	O	O
investigated	153,165	O	O
for	166,169	O	O
naturally	170,179	O	B-Material
occurring	180,189	O	I-Material
elemental	190,199	O	I-Material
impurities	200,210	O	I-Material
found	211,216	O	O
in	217,219	O	O
limestone	220,229	O	B-Material
,	229,230	O	O
that	231,235	O	O
have	236,240	O	O
the	241,244	O	O
potential	245,254	O	O
to	255,257	O	O
be	258,260	O	O
released	261,269	O	O
to	270,272	O	O
the	273,276	O	B-Material
environment	277,288	O	I-Material
under	289,294	O	O
carbonation	295,306	O	B-Process
and	307,310	O	I-Process
calcination	311,322	O	I-Process
conditions	323,333	O	I-Process
.	333,334	O	O

Inductively	335,346	O	B-Method
Coupled	347,354	O	I-Method
Plasma	355,361	O	I-Method
-	361,362	O	I-Method
Mass	362,366	O	I-Method
Spectrometry	367,379	O	I-Method
(	380,381	O	I-Method
ICP	381,384	O	I-Method
-	384,385	O	I-Method
MS	385,387	O	I-Method
)	387,388	O	I-Method
analysis	389,397	O	I-Method
of	398,400	O	O
Longcliffe	401,411	O	B-Material
SP52	412,416	O	I-Material
limestone	417,426	O	I-Material
was	427,430	O	O
undertaken	431,441	O	O
to	442,444	O	O
identify	445,453	O	O
other	454,459	O	B-Material
impurities	460,470	O	I-Material
present	471,478	O	O
,	478,479	O	O
and	480,483	O	O
the	484,487	O	B-Process
effect	488,494	O	I-Process
of	495,497	O	O
sorbent	498,505	O	B-Data
mass	506,510	O	I-Data
and	511,514	O	O
SO2	515,518	O	B-Data
concentration	519,532	O	I-Data
on	533,535	O	O
elemental	536,545	O	B-Process
partitioning	546,558	O	I-Process
in	559,561	O	O
the	562,565	O	B-Material
carbonator	566,576	O	I-Material
between	577,584	O	O
solid	585,590	O	B-Material
sorbent	591,598	O	I-Material
and	599,602	O	O
gaseous	603,610	O	B-Material
phase	611,616	O	I-Material
was	617,620	O	O
investigated	621,633	O	O
,	633,634	O	O
using	635,640	O	O
a	641,642	O	B-Material
bubbler	643,650	O	I-Material
sampling	651,659	O	I-Material
system	660,666	O	I-Material
.	666,667	O	O

Samples	668,675	O	B-Material
were	676,680	O	O
analysed	681,689	O	B-Process
using	690,695	O	O
ICP	696,699	O	B-Method
-	699,700	O	I-Method
MS	700,702	O	I-Method
,	702,703	O	O
which	704,709	O	O
showed	710,716	O	O
that	717,721	O	O
sorbent	722,729	O	B-Data
mass	730,734	O	I-Data
and	735,738	O	O
SO2	739,742	O	B-Data
concentration	743,756	O	I-Data
in	757,759	O	O
the	760,763	O	B-Material
carbonator	764,774	O	I-Material
effected	775,783	O	O
the	784,787	O	B-Data
concentrations	788,802	O	I-Data
of	803,805	O	O
gaseous	806,813	O	B-Material
trace	814,819	O	I-Material
elements	820,828	O	I-Material
sampled	829,836	O	I-Material
.	836,837	O	O

Thermodynamic	838,851	O	B-Method
modelling	852,861	O	I-Method
of	862,864	O	O
the	865,868	O	B-Process
carbonation	869,880	O	I-Process
and	881,884	O	I-Process
calcination	885,896	O	I-Process
process	897,904	O	I-Process
was	905,908	O	O
also	909,913	O	O
undertaken	914,924	O	O
,	924,925	O	O
based	926,931	O	O
on	932,934	O	O
molar	935,940	O	B-Data
quantities	941,951	O	I-Data
of	952,954	O	O
trace	955,960	O	B-Material
elements	961,969	O	I-Material
identified	970,980	O	O
from	981,985	O	O
ICP	986,989	O	B-Method
-	989,990	O	I-Method
MS	990,992	O	I-Method
analysis	993,1001	O	I-Method
of	1002,1004	O	O
limestone	1005,1014	O	B-Material
,	1014,1015	O	O
which	1016,1021	O	O
provided	1022,1030	O	O
useful	1031,1037	O	O
information	1038,1049	O	O
with	1050,1054	O	O
regards	1055,1062	O	O
to	1063,1065	O	O
element	1066,1073	O	B-Data
stability	1074,1083	O	I-Data
and	1084,1087	O	O
partitioning	1088,1100	O	B-Process
under	1101,1106	O	O
realistic	1107,1116	O	B-Data
CO2	1117,1120	O	I-Data
capture	1121,1128	O	I-Data
conditions	1129,1139	O	I-Data
.	1139,1140	O	O

-DOCSTART- (S0301010413004096)

Metal	0,5	O	B-Material
-	5,6	O	I-Material
organic	6,13	O	I-Material
framework	14,23	O	I-Material
(	24,25	O	I-Material
MOF	25,28	O	I-Material
)	28,29	O	I-Material
materials	30,39	O	I-Material
show	40,44	O	O
promise	45,52	O	O
for	53,56	O	O
H2	57,59	O	B-Process
storage	60,67	O	I-Process

and	68,71	O	O
it	72,74	O	O
is	75,77	O	O
widely	78,84	O	O
predicted	85,94	O	B-Process
by	95,97	O	O
computational	98,111	O	B-Process
modelling	112,121	O	I-Process
that	122,126	O	O
MOFs	127,131	O	B-Material
incorporating	132,145	O	O
ultra	146,151	O	B-Material
-	151,152	O	I-Material
micropores	152,162	O	I-Material
are	163,166	O	O
optimal	167,174	O	B-Data
for	175,178	O	O
H2	179,181	O	B-Process
binding	182,189	O	I-Process
due	190,193	O	O
to	194,196	O	O
enhanced	197,205	O	B-Data
overlapping	206,217	O	I-Data
potentials	218,228	O	I-Data
.	228,229	O	O

We	230,232	O	O
report	233,239	O	O
the	240,243	O	O
investigation	244,257	O	O
using	258,263	O	O
inelastic	264,273	O	B-Process
neutron	274,281	O	I-Process
scattering	282,292	O	I-Process
of	293,295	O	O
the	296,299	O	B-Process
interaction	300,311	O	I-Process
of	312,314	O	O
H2	315,317	O	B-Material
in	318,320	O	O
an	321,323	O	B-Material
ultra	324,329	O	I-Material
-	329,330	O	I-Material
microporous	330,341	O	I-Material
MOF	342,345	O	I-Material
material	346,354	O	I-Material
showing	355,362	O	O
low	363,366	O	B-Data
H2	367,369	O	B-Data
uptake	370,376	O	I-Data
capacity	377,385	O	I-Data
.	385,386	O	O

The	387,390	O	O
study	391,396	O	O
has	397,400	O	O
revealed	401,409	O	O
that	410,414	O	O
adsorbed	415,423	O	B-Material
H2	424,426	O	I-Material
at	427,429	O	O
5	430,431	O	B-Data
K	431,432	O	I-Data
has	433,436	O	O
a	437,438	O	B-Process
liquid	439,445	O	I-Process
recoil	446,452	O	I-Process
motion	453,459	O	I-Process
along	460,465	O	O
the	466,469	O	B-Material
channel	470,477	O	I-Material
with	478,482	O	O
very	483,487	O	B-Process
little	488,494	O	I-Process
interaction	495,506	O	I-Process
with	507,511	O	O
the	512,515	O	B-Material
MOF	516,519	O	I-Material
host	520,524	O	I-Material
,	524,525	O	O
consistent	526,536	O	O
with	537,541	O	O
the	542,545	O	O
observed	546,554	O	O
low	555,558	O	B-Data
uptake	559,565	O	B-Process
.	565,566	O	O

The	567,570	O	B-Data
low	571,574	O	I-Data
H2	575,577	O	B-Process
uptake	578,584	O	I-Process
is	585,587	O	O
not	588,591	O	O
due	592,595	O	O
to	596,598	O	O
incomplete	599,609	O	B-Process
activation	610,620	O	I-Process
or	621,623	O	I-Process
decomposition	624,637	O	I-Process
as	638,640	O	O
the	641,644	O	B-Material
desolvated	645,655	O	I-Material
MOF	656,659	O	I-Material
shows	660,665	O	O
CO2	666,669	O	B-Process
uptake	670,676	O	I-Process
with	677,681	O	O
a	682,683	O	B-Data
measured	684,692	O	I-Data
pore	693,697	O	I-Data
volume	698,704	O	I-Data
close	705,710	O	O
to	711,713	O	O
that	714,718	O	O
of	719,721	O	O
the	722,725	O	B-Data
single	726,732	O	I-Data
crystal	733,740	O	I-Data
pore	741,745	O	I-Data
volume	746,752	O	I-Data
.	752,753	O	O

This	754,758	O	O
study	759,764	O	O
represents	765,775	O	O
a	776,777	O	O
unique	778,784	O	O
example	785,792	O	O
of	793,795	O	O
surprisingly	796,808	O	O
low	809,812	O	B-Data
H2	813,815	O	B-Process
uptake	816,822	O	I-Process
within	823,829	O	O
a	830,831	O	B-Material
MOF	832,835	O	I-Material
material	836,844	O	I-Material
,	844,845	O	O
and	846,849	O	O
complements	850,861	O	O
the	862,865	O	O
wide	866,870	O	O
range	871,876	O	O
of	877,879	O	O
studies	880,887	O	O
on	888,890	O	O
systems	891,898	O	O
showing	899,906	O	O
higher	907,913	O	B-Data
uptake	914,920	O	B-Data
capacities	921,931	O	I-Data
and	932,935	O	O
binding	936,943	O	B-Process
interactions	944,956	O	I-Process
.	956,957	O	O

