-DOCSTART- (S0021979713004438)

A	0,1	O	O
range	2,7	O	O
of	8,10	O	O
in	11,13	O	B-Method
situ	14,18	O	I-Method
analytical	19,29	O	B-Process
techniques	30,40	O	I-Process
and	41,44	O	O
theoretical	45,56	O	B-Process
calculations	57,69	O	I-Process
were	70,74	O	O
applied	75,82	O	O
to	83,85	O	O
gain	86,90	O	O
insights	91,99	O	O
into	100,104	O	O
the	105,108	O	B-Process
formation	109,118	O	I-Process
and	119,122	O	O
properties	123,133	O	B-Data
of	134,136	O	O
the	137,140	O	B-Material
Mefp-1	141,147	O	I-Material
film	148,152	O	I-Material
on	153,155	O	O
iron	156,160	O	B-Material
substrate	161,170	O	I-Material
,	170,171	O	O
as	172,174	O	O
well	175,179	O	O
as	180,182	O	O
the	183,186	O	B-Process
protein	187,194	O	I-Process
complexation	195,207	O	I-Process
with	208,212	O	O
Fe3	213,216	O	B-Material
+	216,217	O	I-Material
ions	218,222	O	I-Material
.	222,223	O	O

Adsorption	224,234	O	B-Data
kinetics	235,243	O	I-Data
of	244,246	O	O
Mefp-1	247,253	O	B-Material
and	254,257	O	O
the	258,261	O	B-Process
complexation	262,274	O	I-Process
were	275,279	O	O
investigated	280,292	O	O
using	293,298	O	O
QCM	299,302	O	B-Method
-	302,303	O	I-Method
D.	303,305	O	I-Method

The	306,309	O	O
results	310,317	O	O
suggest	318,325	O	O
an	326,328	O	B-Process
initially	329,338	O	I-Process
fast	339,343	O	I-Process
adsorption	344,354	O	I-Process
,	354,355	O	O
with	356,360	O	O
the	361,364	O	B-Material
molecules	365,374	O	I-Material
oriented	375,383	O	B-Process
preferentially	384,398	O	I-Process
parallel	399,407	O	I-Process
to	408,410	O	O
the	411,414	O	B-Material
surface	415,422	O	I-Material
,	422,423	O	O
followed	424,432	O	O
by	433,435	O	O
a	436,437	O	B-Process
structural	438,448	O	I-Process
change	449,455	O	I-Process
within	456,462	O	O
the	463,466	O	B-Material
film	467,471	O	I-Material
leading	472,479	O	O
to	480,482	O	O
molecules	483,492	O	B-Material
extending	493,502	O	O
toward	503,509	O	O
solution	510,518	O	B-Material
.	518,519	O	O

Exposure	520,528	O	B-Process
to	529,531	O	O
a	532,533	O	B-Material
diluted	534,541	O	I-Material
FeCl3	542,547	O	I-Material
solution	548,556	O	I-Material
results	557,564	O	O
in	565,567	O	O
enhanced	568,576	O	B-Process
complexation	577,589	O	I-Process
within	590,596	O	O
the	597,600	O	B-Material
adsorbed	601,609	O	I-Material
protein	610,617	O	I-Material
film	618,622	O	I-Material
,	622,623	O	O
leading	624,631	O	O
to	632,634	O	O
water	635,640	O	B-Process
removal	641,648	O	I-Process
and	649,652	O	O
film	653,657	O	B-Process
compaction	658,668	O	I-Process
.	668,669	O	O

In	670,672	O	B-Method
situ	673,677	O	I-Method
Peak	678,682	O	I-Method
Force	683,688	O	I-Method
Tapping	689,696	O	I-Method
AFM	697,700	O	I-Method
was	701,704	O	O
employed	705,713	O	O
for	714,717	O	O
determining	718,729	O	B-Process
morphology	730,740	O	B-Data
and	741,744	O	I-Data
nano	745,749	O	I-Data
-	749,750	O	I-Data
mechanical	750,760	O	I-Data
properties	761,771	O	I-Data
of	772,774	O	O
the	775,778	O	B-Material
surface	779,786	O	I-Material
layer	787,792	O	I-Material
.	792,793	O	O

The	794,797	O	O
results	798,805	O	O
,	805,806	O	O
in	807,809	O	O
agreement	810,819	O	O
with	820,824	O	O
the	825,828	O	B-Method
QCM	829,832	O	I-Method
-	832,833	O	I-Method
D	833,834	O	I-Method
observations	835,847	O	O
,	847,848	O	O
demonstrate	849,860	O	O
that	861,865	O	O
addition	866,874	O	B-Process
of	875,877	O	O
Fe3	878,881	O	B-Material
+	881,882	O	I-Material
induces	883,890	O	B-Process
a	891,892	O	B-Process
transition	893,903	O	I-Process
from	904,908	O	O
an	909,911	O	B-Material
extended	912,920	O	I-Material
and	921,924	O	I-Material
soft	925,929	O	I-Material
protein	930,937	O	I-Material
layer	938,943	O	I-Material
to	944,946	O	O
a	947,948	O	B-Material
denser	949,955	O	I-Material
and	956,959	O	I-Material
stiffer	960,967	O	I-Material
one	968,971	O	I-Material
.	971,972	O	O

Further	973,980	O	O
,	980,981	O	O
in	982,984	O	B-Method
situ	985,989	O	I-Method
ATR	990,993	O	I-Method
-	993,994	O	I-Method
FTIR	994,998	O	I-Method
and	999,1002	O	O
Confocal	1003,1011	O	B-Method
Raman	1012,1017	O	I-Method
Micro	1018,1023	O	I-Method
-	1023,1024	O	I-Method
spectroscopy	1024,1036	O	I-Method
(	1037,1038	O	I-Method
CRM	1038,1041	O	I-Method
)	1041,1042	O	I-Method
techniques	1043,1053	O	O
were	1054,1058	O	O
utilized	1059,1067	O	O
to	1068,1070	O	O
monitor	1071,1078	O	B-Process
compositional	1079,1092	O	B-Process
/	1092,1093	O	I-Process
structural	1093,1103	O	I-Process
changes	1104,1111	O	I-Process
in	1112,1114	O	O
the	1115,1118	O	B-Material
surface	1119,1126	O	I-Material
layer	1127,1132	O	I-Material
due	1133,1136	O	O
to	1137,1139	O	O
addition	1140,1148	O	B-Process
of	1149,1151	O	O
Fe3	1152,1155	O	B-Material
+	1155,1156	O	I-Material
ions	1157,1161	O	I-Material
.	1161,1162	O	O

The	1163,1166	O	B-Process
spectroscopic	1167,1180	O	I-Process
analyses	1181,1189	O	I-Process
assisted	1190,1198	O	O
by	1199,1201	O	O
DFT	1202,1205	O	B-Process
calculations	1206,1218	O	I-Process
provide	1219,1226	O	O
evidence	1227,1235	O	O
for	1236,1239	O	O
formation	1240,1249	O	B-Process
of	1250,1252	O	O
tri	1253,1256	O	B-Material
-	1256,1257	O	I-Material
Fe3+/catechol	1257,1270	O	I-Material
complexes	1271,1280	O	I-Material
in	1281,1283	O	O
the	1284,1287	O	B-Material
surface	1288,1295	O	I-Material
film	1296,1300	O	I-Material
,	1300,1301	O	O
which	1302,1307	O	O
is	1308,1310	O	O
enhanced	1311,1319	O	B-Process
by	1320,1322	O	O
Fe3	1323,1326	O	B-Process
+	1326,1327	O	I-Process
addition	1328,1336	O	I-Process
.	1336,1337	O	O

-DOCSTART- (S1387700313001822)

The	0,3	O	B-Process
path	4,8	O	I-Process
of	9,11	O	I-Process
the	12,15	O	I-Process
chirality	16,25	O	I-Process
delivery	26,34	O	I-Process
in	35,37	O	O
the	38,41	O	B-Material
crystalline	42,53	O	I-Material
and	54,57	O	I-Material
chiral	58,64	O	I-Material
nucleotide	65,75	O	I-Material
-	75,76	O	I-Material
Co(II	76,81	O	I-Material
)	81,82	O	I-Material
complex	83,90	O	I-Material
,	90,91	O	I-Material
[	92,93	O	I-Material
Co(GMP)(H2O)5]·3H2O	93,112	O	I-Material
(	113,114	O	I-Material
GMP	114,117	O	I-Material
=	117,118	O	I-Material
guanosine-5′-monophosphate	118,144	O	I-Material
)	144,145	O	I-Material
,	145,146	O	O
has	147,150	O	O
been	151,155	O	O
studied	156,163	O	O
based	164,169	O	O
on	170,172	O	O
X	173,174	O	B-Method
-	174,175	O	I-Method
ray	175,178	O	I-Method
single	179,185	O	I-Method
crystal	186,193	O	I-Method
diffraction	194,205	O	I-Method
analysis	206,214	O	I-Method
,	214,215	O	O
liquid-	216,223	O	B-Method
and	224,227	O	I-Method
solid	228,233	O	I-Method
-	233,234	O	I-Method
state	234,239	O	I-Method
circular	240,248	O	I-Method
dichroism	249,258	O	I-Method
(	259,260	O	I-Method
CD	260,262	O	I-Method
)	262,263	O	I-Method
spectroscopy	264,276	O	I-Method
.	276,277	O	O

The	278,281	O	B-Process
multiple	282,290	O	I-Process
and	291,294	O	I-Process
helical	295,302	O	I-Process
H	303,304	O	I-Process
-	304,305	O	I-Process
bonding	305,312	O	I-Process
is	313,315	O	O
a	316,317	O	O
distinctive	318,329	O	O
way	330,333	O	O
of	334,336	O	O
chirality	337,346	O	B-Process
delivery	347,355	O	I-Process
from	356,360	O	O
the	361,364	O	B-Material
discrete	365,373	O	I-Material
molecules	374,383	O	I-Material
to	384,386	O	O
three	387,392	O	B-Material
-	392,393	O	I-Material
dimensional	393,404	O	I-Material
supramolecular	405,419	O	I-Material
architecture	420,432	O	I-Material
.	432,433	O	O

Graphical	434,443	O	O
abstract	444,452	O	O

The	453,456	O	B-Process
molecular	457,466	O	I-Process
chirality	467,476	O	I-Process
of	477,479	O	O
crystallized	480,492	O	B-Material
nucleotide	493,503	O	I-Material
-	503,504	O	I-Material
transition	504,514	O	I-Material
metal	515,520	O	I-Material
coordination	521,533	O	I-Material
complex	534,541	O	I-Material
is	542,544	O	O
delivered	545,554	O	O
to	555,557	O	O
three	558,563	O	B-Material
-	563,564	O	I-Material
dimensional	564,575	O	I-Material
supramolecular	576,590	O	I-Material
architecture	591,603	O	I-Material
through	604,611	O	O
multiple	612,620	O	B-Process
and	621,624	O	I-Process
helical	625,632	O	I-Process
H	633,634	O	I-Process
-	634,635	O	I-Process
bonding	635,642	O	I-Process
in	643,645	O	O
its	646,649	O	B-Material
crystal	650,657	O	I-Material
lattice	658,665	O	I-Material
.	665,666	O	O

Liquid-	667,674	O	B-Method
and	675,678	O	I-Method

solid	679,684	O	I-Method
-	684,685	O	I-Method
state	685,690	O	I-Method
CD	691,693	O	I-Method
spectroscopy	694,706	O	I-Method
confirm	707,714	O	O
the	715,718	O	B-Process
novel	719,724	O	I-Process
path	725,729	O	I-Process
of	730,732	O	I-Process
chirality	733,742	O	I-Process
delivery	743,751	O	I-Process
.	751,752	O	O

Highlights	753,763	O	O
•	764,765	O	O

The	766,769	O	B-Process
chirality	770,779	O	I-Process
delivery	780,788	O	I-Process
in	789,791	O	O
the	792,795	O	B-Material
crystallized	796,808	O	I-Material
coordination	809,821	O	I-Material
complex	822,829	O	I-Material
has	830,833	O	O
been	834,838	O	O
studied	839,846	O	O
.	846,847	O	O

•	848,849	O	O

The	850,853	O	B-Process
path	854,858	O	I-Process
of	859,861	O	I-Process
chirality	862,871	O	I-Process
delivery	872,880	O	I-Process
in	881,883	O	O
this	884,888	O	B-Material
complex	889,896	O	I-Material
is	897,899	O	O
multiple	900,908	O	B-Process
and	909,912	O	I-Process
helical	913,920	O	I-Process
H	921,922	O	I-Process
-	922,923	O	I-Process
bonding	923,930	O	I-Process
.	930,931	O	O

•	932,933	O	O

Liquid-	934,941	O	B-Method

and	942,945	O	I-Method
solid	946,951	O	I-Method
-	951,952	O	I-Method
state	952,957	O	I-Method
CD	958,960	O	I-Method
spectroscopy	961,973	O	I-Method
confirm	974,981	O	O
the	982,985	O	B-Process
mechanism	986,995	O	I-Process
of	996,998	O	I-Process
chirality	999,1008	O	I-Process
delivery	1009,1017	O	I-Process
.	1017,1018	O	O

