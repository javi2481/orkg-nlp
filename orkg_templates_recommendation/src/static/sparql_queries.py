TEMPLATES_INFORMATION_QUERY = """PREFIX orkgc: <http://orkg.org/orkg/class/>
PREFIX orkgp: <http://orkg.org/orkg/predicate/>

SELECT ?template ?templateLabel ?templateComponent
       ?templateComponentProperty ?templateComponentPropertyLabel
       ?templateComponentOccurrenceMin ?templateComponentOccurrenceMax
       ?templateOfClass ?templateOfClassLabel ?templateOfPredicate
       ?templateOfPredicateLabel ?templateOfResearchField ?templateOfResearchFieldLabel
       ?templateOfResearchProblem ?templateOfResearchProblemLabel
       ?templateLabelFormat ?templateStrict
WHERE {
    ?template rdf:type orkgc:ContributionTemplate ;
              rdfs:label ?templateLabel ;
              orkgp:TemplateComponent ?templateComponent .

    ?templateComponent orkgp:TemplateComponentProperty ?templateComponentProperty .
    ?templateComponentProperty  rdfs:label ?templateComponentPropertyLabel .

     OPTIONAL { ?templateComponent orkgp:TemplateComponentOccurrenceMin ?templateComponentOccurrenceMin } .
     OPTIONAL { ?templateComponent orkgp:TemplateComponentOccurrenceMax ?templateComponentOccurrenceMax } .

     OPTIONAL { ?template orkgp:TemplateOfClass ?templateOfClass .
                ?templateOfClass rdfs:label ?templateOfClassLabel } .

     OPTIONAL { ?template orkgp:TemplateOfPredicate ?templateOfPredicate .
                ?templateOfPredicate  rdfs:label ?templateOfPredicateLabel } .

     OPTIONAL { ?template orkgp:TemplateOfResearchField ?templateOfResearchField .
                ?templateOfResearchField rdfs:label ?templateOfResearchFieldLabel } .

     OPTIONAL { ?template orkgp:TemplateOfResearchProblem ?templateOfResearchProblem .
                ?templateOfResearchProblem rdfs:label ?templateOfResearchProblemLabel } .

     OPTIONAL { ?template orkgp:TemplateLabelFormat ?templateLabelFormat } .
     OPTIONAL { ?template orkgp:TemplateStrict ?templateStrict } .
}"""

CONTRIBUTION_PREDICATES_QUERY = """PREFIX orkgp: <http://orkg.org/orkg/predicate/>
PREFIX orkgc: <http://orkg.org/orkg/class/>

SELECT ?contribution ?predicate_1 ?object ?predicate_2 
        WHERE {
            ?contribution rdf:type orkgc:Contribution ;
                          ?predicate_1 ?object .

            OPTIONAL { ?object ?predicate_2 ?anything } .
        }"""


PAPERS_QUERY = """SELECT ?paper ?paper_title ?doi ?research_field ?research_field_label
        WHERE {
               ?paper rdf:type <http://orkg.org/orkg/class/Paper> ;
                      rdfs:label ?paper_title ;
                      <http://orkg.org/orkg/predicate/P30> ?research_field .
               ?research_field rdfs:label ?research_field_label .
               OPTIONAL { ?paper <http://orkg.org/orkg/predicate/P26> ?doi } .
        }
"""


def CONTRIBUTION_INFORMATION_QUERY(contribution_id):
    return """SELECT ?research_field ?research_field_label ?research_problems
        WHERE {{
               ?paper <http://orkg.org/orkg/predicate/P31> {} ;
                      <http://orkg.org/orkg/predicate/P30> ?research_field .
               ?research_field rdfs:label ?research_field_label .
               OPTIONAL {{ {} <http://orkg.org/orkg/predicate/P32> ?research_problems }}
        }}""".format(contribution_id, contribution_id)


def CONTRIBUTION_PAPER(contribution_id):
    return """SELECT ?paper ?paper_title ?doi ?research_field ?research_field_label
        WHERE {{
               ?paper <http://orkg.org/orkg/predicate/P31> {} ;
                      rdfs:label ?paper_title ;
                      <http://orkg.org/orkg/predicate/P30> ?research_field .
               ?research_field rdfs:label ?research_field_label .
               OPTIONAL {{ ?paper <http://orkg.org/orkg/predicate/P26> ?doi }} .
        }}""".format(contribution_id)


def PAPERS_BY_RESEARCH_FIELD_QUERY(research_field):
    return """SELECT ?paper ?paper_title ?doi
        WHERE {{
               ?paper rdf:type <http://orkg.org/orkg/class/Paper> ;
                      rdfs:label ?paper_title ;
                      <http://orkg.org/orkg/predicate/P30> {}
               OPTIONAL {{ ?paper <http://orkg.org/orkg/predicate/P26> ?doi }} .
        }}""".format(research_field)
