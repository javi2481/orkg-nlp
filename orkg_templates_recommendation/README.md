# ORKG's Template Recommendation Service
This directory contains the data and scripts used to build the Template Recommendation Service in the ORKG within the scope of a master's thesis.

## Theoretical Overview
We defined the problem as an NLI (Natural Language Inference) task where each (premise, hypothesis) pair is equivalent to a (template, paper) one.
Because of the problem of the lack of templated papers in the ORKG, a feasibility analysis has been performed as the first step of this work. For doing that and because of the technical problem mentioned in [this issue](https://gitlab.com/TIBHannover/orkg/orkg-ontology/-/issues/3), a heuristic has been developed to retrieve templated papers from the ORKG.
Note that this heuristic should be replaced in the future with a simple SPARQL or Cypher query. 

## How to Run
with python 3.8.10
```bash
git clone https://gitlab.com/TIBHannover/orkg/orkg-nlp.git
cd orkg-nlp
pip install -r requirements.txt   
python -m orkg_templates_recommendation.src.main
```
Executing the previous commands leads to run the following scripts consecutively:
* `dump_reader.py`: retrieves all templates and all contributions from the ORKG.
* `feasibility_test.py`: finds templated contributions.
* `dataset.py`: converts contributions to papers and fetches their abstracts.
* `split_dataset.py`: splits the dataset into training/validation/test sets.

Each script outputs its own file and stores it in `/orkg_templates_recommendation/output`
to be then used as an input to the next script. The latest version of the output data is to be found in `/orkg_templates_recommendation/data`.

## How to Train
* Upload the files `training_set.json` `validation_set.json` `test_set.json` manually to Google Drive.
* Open the notebook `orkg_templates_recommendation/src/templates_recommendation_training.ipynb` in Google Colab.
* Search for TODOs and resolve them.
* Run the notebook on a TPU environment.
The trained model should be uploaded to `your own` bucket on Google Cloud Storage.

## How to Evaluate
* Download the trained model.
* Replace the existing one in the project [orkg-similarity](https://gitlab.com/TIBHannover/orkg/orkg-similarity/)
* Adjust the model file name in `bert.py` in `orkg-similarity`.
* Run the project on your localhost. Please refer to [orkg-similarity](https://gitlab.com/TIBHannover/orkg/orkg-similarity/) for running instructions.
* Run the following command
```bash
python -m orkg_templates_recommendation.src.evaluate -p
```

## How to Configure
The RDF dump of the ORKG in `orkg_templates_recommendation/data/dump.nt` is from 13.08.2021. You may download the current data dump from [here](https://orkg.org/orkg/api/rdf/dump) and replace the file.

Changing the output directory, simcomp host for the evaluation as well as the thresholds used in the scripts,
can be done by changing the respective variable value in the respective script.

The variables are listed at the beginning of each script under the imports statements.

## Contributors
This work was conducted within the scope of the master's thesis **Information Retrieval Service Aspects of the Open Research Knowledge Graph**
by:
* Master's student: Omar Arab Oghli `omar.araboghli@outlook.com`
* Supervisor: Dr. Jennifer D'Souza `jennifer.dsouza@tib.eu`